package fr.lyneteam.nico.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import fr.lyneteam.nico.accounts.LyneTeamAccount;
import fr.lyneteam.nico.sms.SMSDataError.Responce;

public class PreparedSMS {
	private final List<String> numbers;
	private final List<SMS> messages;
	
	public PreparedSMS() {
		this.numbers = new ArrayList<String>();
		this.messages = new ArrayList<SMS>();
	}
	
	public void addNumbers(String... numbers) {
		for (String number : numbers) if (number.length() < 10) this.numbers.add("+33" + number); else if (number.startsWith("+")) this.numbers.add(number); else this.numbers.add("+" + number);
	}
	
	public void removeNumbers(String... numbers) {
		for (String number : numbers) if (number.length() < 10) this.numbers.remove("+33" + number); else if (number.startsWith("+")) this.numbers.remove(number); else this.numbers.remove("+" + number);
	}
	
	public void removeNumbers(int... numbers) {
		for (int number : numbers) this.numbers.remove(number);
	}
	
	public final List<String> getNumbers() {
		return new ArrayList<String>(this.numbers);
	}
	
	public void addMessages(SMS... messages) {
		for (SMS message : messages) this.messages.add(message);
	}
	
	public void removeMessages(SMS... messages) {
		for (SMS message : messages) this.messages.remove(message);
	}
	
	public void removeMessages(int... messages) {
		for (int message : messages) this.messages.remove(message);
	}
	
	public final List<SMS> getMessages() {
		return new ArrayList<SMS>(this.messages);
	}
	
	public final boolean send(LyneTeamAccount account) throws SMSDataError {
		try {
			URL url = new URL("https://lyneteam.eu/sms.php?id=" + account.getID() + "&ip=" + account.getAuthClass().getIP());
			String numbers = "";
			for (String number : this.numbers) numbers = numbers + ";" + number;
			numbers = numbers.substring(1);
			String messages = "";
			for (SMS sms : this.messages) if (!sms.getLines().isEmpty()) {
				String message = "";
				for (String line : sms.getLines()) message = message + "<line>" + line;
				message = message.substring(6);
				messages = messages + "<message>" + message;
			}
			messages = messages.substring(9);
			String post = "numbers=" + URLEncoder.encode(numbers, "UTF-8") + "&messages=" + URLEncoder.encode(messages, "UTF-8");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(post);
			writer.flush();
			try {
				writer.close();
			} catch (Exception exception) {
				// Ignore
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String responce = reader.readLine();
			try {
				reader.close();
			} catch (Exception exception) {
				// Ignore
			}
			try {
				int id = Integer.parseInt(responce);
				if (id == 8) return true;
				throw new SMSDataError(Responce.getResponceByID(id));
			} catch (SMSDataError exception) {
				throw exception;
			}
		} catch (SMSDataError exception) {
			throw exception;
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new SMSDataError(exception);
		}
	}
}