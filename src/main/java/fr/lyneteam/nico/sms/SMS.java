package fr.lyneteam.nico.sms;

import java.util.ArrayList;
import java.util.List;

public class SMS {
	private final List<String> lines;
	
	public SMS(String... lines) {
		this.lines = new ArrayList<String>();
		this.addLines(lines);
	}
	
	public void addLines(String... lines) {
		for (String line : lines) this.lines.add(line);
	}
	
	public void removeLines(String... lines) {
		for (String line : lines) this.lines.remove(line);
	}
	
	public void removeLines(int... lines) {
		for (int line : lines) this.lines.remove(line);
	}
	
	public final String getLine(int line) {
		return this.lines.get(line);
	}
	
	public final List<String> getLines() {
		return this.lines;
	}
}